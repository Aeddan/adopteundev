import json

identifiant = input("Entrer votre ID ")
firstName = input("Entrer votre prénom ")
lastName = input("Entrer votre nom ")
birthDay = input("Entrer votre date de naissance au format JJ/MM/AAAA ")
mail = input("Entrer votre mail ")
phone = input("Entrer votre numéro de téléphone ")
image = "profil.png"

print("Entrer vos réseaux sociaux, laisser vide si nul ")
fb = input("Entrer votre lien fb ")
lkdn = input("Entrer votre lien LinkedIn ")
twtr = input("Entrer votre lien Twitter ")
insta = input("Entrer votre lien Instagram ")
git = input("Entrer votre lien Git ")
so = input("Entrer votre lien StackOverflow ")


reseaux = []
autreReseau = ""
while autreReseau != "N":
    
    autreReseau = input("Avez vous d'autres réseau à ajouter? (O/N)")
    if autreReseau == "O":
        socials = {}
        nomReseau = input("Enter le nom du réseau ")
        lienReseau = input("Entrer le lien du réseau ")
        socials["name"]=nomReseau
        socials["link"]=lienReseau
        reseaux.append(socials)

pdfCV = "cv.pdf"
linkCV = input("Entrer le lien de votre CV ")

realisations = []
autreRealisation = ""
while autreRealisation != "N":
    
    autreRealisation = input("Avez vous d'autres realisations à ajouter? (O/N) ")
    if autreRealisation == "O":
        achievements = {}
        nomRealisation = input("Enter le nom de la realisation ")
        snapshot = nomRealisation + ".png"
        lienRealisation = input("Entrer le lien de la realisation ")
        desc = input("Entrer une description de votre réalisation ")
        achievements["snapshot"] = snapshot
        achievements["link"] = lienRealisation
        achievements["name"] = nomRealisation
        achievements["desc"] = desc
        realisations.append(achievements)
video = "video.mp4"


badges = []
autreBadge = ""
while autreBadge != "N":
    autreBadge = input("Avez vous d'autres badges à ajouter? (O/N) ")
    if autreBadge == "O":
        dictionnary = {}
        idBadge = input("Entrer l'Id du badge ")

        enabled = ""
        while enabled != "O" and enabled != "N":
            enabled = input("Avez vous déjà ce badge? (O/N) ")
        boolean = False
        if enabled == "O":
            boolean = True
        dictionnary["id"] = int(idBadge)
        dictionnary["enabled"] = boolean
        badges.append(dictionnary)

status = ""
while status != "O" and status != "N":
    status = input("Avez vous trouvé votre alternance? (O/N) ")
adopted = False
if status == "O":
    adopted = True
promo = "A2 Dev"

comments = []

convert = {
    "id": int(identifiant),
    "contact": {
        "firstName": firstName,
        "lastName": lastName,
        "birthday": birthDay,
        "mail": mail,
        "phone": phone,
        "img": image
    },
    "social": {
        "fb": fb,
        "lkdn": lkdn,
        "twtr": twtr,
        "insta": insta,
        "git": git,
        "so": so,
        "Others": reseaux
    },
    "medias": {
        "pdfCV": pdfCV,
        "linkCV": linkCV,
        "realisations": realisations,
        "video": video
    },
    "badges": badges,
    "status": {
        "adopted": adopted,
        "promo": promo,
    },
    "comments": comments
}

text = identifiant + ".json"

with open(text, 'w') as outfile:
    json.dump(convert, outfile)